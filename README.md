# Android KeyStore #

Sample code how to use Android KeyStore to store cryptographic key.

### What is this repository for? ###

* Android Credentials Storage Method
* Small part of the original: https://github.com/nelenkov/android-keystore

### How do I get set up? ###

* Git clone
* Open with Android Studio (gradle project)
* Run
* Enjoy our test